<?php

/**
 * @file
 * Administration page callbacks for the Adaptive BG module.
 */

function adaptive_bg_admin_form($form, &$form_state) {

  $form['selectors'] = array(
    '#type' => 'fieldset',
    '#title' => t('Selectors'),
    '#description' => t('Upload an image to use as the background for the element defined with a valid CSS selector'),
  );

  $form['selectors']['css_selector'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS Selector'),
    '#description' => t('Valid CSS selector.'),
    '#default_value' => 'body .node',
    '#prefix' => '<div class="selector-group-wrapper">',
  );

  $form['selectors']['bg_image'] = array(
    '#type' => 'managed_file',
    '#title' => t('Choose a file'),
    '#upload_location' => 'public://adaptive_bg',
    '#description' => t('Upload an image to use as the background for the element defined above.'),
    '#default_value' => variable_get('adaptive_bg_files'),
      '#suffix' => '</div>',
  );

  return system_settings_form($form);
}

function adaptive_bg_form_alter(&$form, &$form_state, $form_id) {

  $form['#submit'][] = 'adaptive_bg_admin_save';
}

function adaptive_bg_admin_save(&$form, &$form_state) {

  $file = file_load($form_state['values']['bg_image']);
  $file->status = FILE_STATUS_PERMANENT;
  file_save($file);

  file_usage_add($file, 'adaptive_bg', 'managed_file', $file->fid);

  // Temporary test var.
  variable_set('adaptive_bg_files', $file->fid);

}